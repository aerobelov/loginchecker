//
//  unittest.swift
//  unittest
//
//  Created by Pavel Belov on 10.11.2019.
//  Copyright © 2019 Pavel Belov. All rights reserved.
//

import XCTest
@testable import loginChecker


class unittest: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        let c = validator()
        XCTAssertFalse(c.validate("-login"))
        XCTAssertFalse(c.validate(".login"))
        XCTAssertFalse(c.validate("3login"))
        XCTAssertFalse(c.validate("in"))
        XCTAssertFalse(c.validate("qwertyuiopasdfghjklzxcvbnmqwertyu"))
        
        XCTAssertTrue(c.validate("log-in"))
        XCTAssertTrue(c.validate("log.in"))
        XCTAssertTrue(c.validate("log2in"))
        XCTAssertTrue(c.validate("aerobelov@gmail.com"))
        XCTAssertTrue(c.validate("login"))
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        measure {
            // Put the code you want to measure the time of here.
        }
    }

}
