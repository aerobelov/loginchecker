//
//  main.swift
//  loginChecker
//
//  Created by Pavel Belov on 10.11.2019.
//  Copyright © 2019 Pavel Belov. All rights reserved.
//

import Foundation



class validator {
    let login_pattern = #"^[^\.\-\d][a-z\-\.0-9]{3,32}"#
    let email_pattern = #"(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])"#
    
    func validate(_ text: String) -> Bool {
        return  (text.range(of: self.login_pattern, options: .regularExpression) != nil ||
            text.range(of: self.email_pattern, options: .regularExpression) != nil)
    }
}


print("input your login\n")
let test = validator()
if let input = readLine() {
    if (test.validate(input) ||
        test.validate(input))  {
            print ("Validated")
        } else {
            print ("Invalid login")
        }
}
